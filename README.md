Crunckcoin
===================

SHA256N POW
----------

- The starting reward is 44 CRUK
- Halving takes place every 69,000 blocks
- Block average time is **15 seconds**
- Difficulty retargets each block
- Blocks 1 - 5000 pay a reward of **1000** CRUK 
- Blocks 5001 - 11000 pay a reward of **500** CRUK 
- Blocks 11001 - 11200 pay a reward of **500000** CRUK 

Crunckcoin.conf
===================

- username=
- password=
- daemon=1
- listen=1
- rpcallowip=127.0.0.1
- rpcport=8791
- port=8790

